**Bienvenue sur la page de documentation du groupe [`forga`](https://gitlab.com/forga/).**

---


💡 C'est quoi l'idée ?
=====================

[![Vue arborescente du groupe `forga`](../img/forga-tree-s.jpg)](../img/forga-tree-l.png)

Le groupe [`forga`](https://gitlab.com/forga/) a pour objectif de proposer une organisation facilitant :

- ♻️ la réutilisation de code;
- 📝 la documentation (de code ou d'autre chose);
- 🤝 la collaboration;

Il utilise les [groupes](https://docs.gitlab.com/ce/user/group/)/[sous-groupes](https://docs.gitlab.com/ce/user/group/subgroups/) , les [`pages`](https://about.gitlab.com/stages-devops-lifecycle/pages/), et d'autres fonctionnalités de _GitLab_ pour permettre l'organisation :

- de dépôts `git` rassemblé par usages (dans une arborescence);
- de la gestion des accès utilisateurs en lot;
- d'URLs sémantiques pour les pages web statiques de documentation;

Le contenu traite du développement dans un contexte Python & Django, mais tout type de language/_framework_ y trouvera sa place.


⚠️ Avertissement
-----------------

C'est un projet expérimental en cours de développement.

Certains aspects ont été mis en place dans un contexte professionnel, d'autres sont des expérimentations personnelles.


📦 Qu'est ce qu'il y a dedans ?
===============================

Pour l'instant 
--------------

1. un [outil de mentorat](https://gitlab.com/forga/process/fr/embarquement/) des nouveaux utilisateur·ice·s (orienté Python/Django/UML);
1. une [source de documentation unique](https://forga.gitlab.io/process/fr/manuel/) : _le manuel_;
1. des paquets [Django](https://www.djangoproject.com/) :
    - un [démarrage rapide](https://gitlab.com/forga/tool/django/core/);
    - une [application installables directement avec `pip`](https://gitlab.com/forga/tool/django/one_to_one/);
1. un [projet client](https://gitlab.com/forga/customer/acme/) réutilisant les paquets Django mentionnés ci-dessus;
1. des [projets tiers](https://gitlab.com/forga/devel/third-party/) utilisé en interne;


Et sinon vous pouvez toujours [visiter l'URL du groupe](https://gitlab.com/forga/) et dérouler le contenus des sous-groupes pour vous faire une idée.


🤝 Envie de contribuer ?
=======================

Avec plaisir !

Il y a quelques conventions ([git](https://forga.gitlab.io/process/fr/manuel/convention/git/) & [python](https://forga.gitlab.io/process/fr/manuel/convention/python/)) présentées dans [le manuel](https://forga.gitlab.io/process/fr/manuel/) ainsi qu'un [code de conduite](https://forga.gitlab.io/process/fr/manuel/human/code-of-conduct/).

Et pourquoi ne pas proposer une [édition de cette page](https://gitlab.com/forga/-/edit/production/docs/fr/index.md)?


© Crédits
=========

* icônes des (sous)groupes :  [GNOME/adwaita-icon-theme](https://gitlab.gnome.org/GNOME/adwaita-icon-theme/-/blob/production/COPYING);
* icônes _drapeaux_ : [Freepik](https://www.flaticon.com/authors/freepik) sur [www.flaticon.com](https://www.flaticon.com/packs/flags-collection-3);
