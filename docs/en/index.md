**Welcome to the documentation page of the [`forga`](https://gitlab.com/forga/) group.**

---


💡 What's the idea ?
==================

[![Tree view for `forga` group](../img/forga-tree-s.jpg)](../img/forga-tree-l.png)

The [`forga`](https://gitlab.com/forga/) group aims to provide an organization that facilitates :

- ♻️ the reuse of code;
- 📝 documentation (of code or anything else);
- 🤝 collaboration;

It uses the [groups](https://docs.gitlab.com/ce/user/group/)/[subgroups](https://docs.gitlab.com/ce/user/group/subgroups/), [pages](https://about.gitlab.com/stages-devops-lifecycle/pages/), and other _GitLab_ features to enable organization :

- of `git` repositories grouped by usage (in a tree structure);
- batch user access management;
- semantic URLs for static documentation web pages;

The content deals with development in a Python & Django context, but any kind of language/_framework_ will find its place there.


⚠️ Warning
----------

It is an experimental project under development.

Some aspects have been set up in a professional context, others are personal experiments.


📦 What's in it ?
=================

For now
-------

1. an [mentorship tool](https://gitlab.com/forga/process/fr/embarquement/) for new users (French version for Python/Django/UML);
1. a [single source of documentation](https://forga.gitlab.io/process/fr/manuel/)· _le Manuel_ (French version);
1. some [Django](https://www.djangoproject.com/) packages:
    - a [bootstrap project](https://gitlab.com/forga/tool/django/core/);
    - an [application directly installable with `pip`](https://gitlab.com/forga/tool/django/one_to_one/);
1. a [customer project](https://gitlab.com/forga/customer/acme/) reusing the Django packages above;
1. [third-party projects](https://gitlab.com/forga/devel/third-party/) used internally;


And if not, you can always [visit the group URL](https://gitlab.com/forga/) and scroll down the contents of the subgroups to get an idea.


🤝 Want to contribute ?
======================

With pleasure!

There are some conventions ([git](https://forga.gitlab.io/process/fr/manuel/convention/git/) & [python](https://forga.gitlab.io/process/fr/manuel/convention/python/)) presented in [the manual](https://forga.gitlab.io/process/fr/manuel/) and a [code of conduct](https://forga.gitlab.io/process/fr/manuel/human/code-of-conduct/).

And why not propose an [edition of this page](https://gitlab.com/forga/-/edit/production/docs/en/index.md)?


© Credits
=========

* (sub)group icons :  [GNOME/adwaita-icon-theme](https://gitlab.gnome.org/GNOME/adwaita-icon-theme/-/blob/production/COPYING);
* _flag_ icons: [Freepik](https://www.flaticon.com/authors/freepik) on [www.flaticon.com](https://www.flaticon.com/packs/flags-collection-3);
